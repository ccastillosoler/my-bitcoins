module.exports = {
    getPersistenceAdapter() {
        const tableName = 'my_bitcoins_table';
        
        const {S3PersistenceAdapter} = require('ask-sdk-s3-persistence-adapter');
        return new S3PersistenceAdapter({ 
            bucketName: process.env.S3_PERSISTENCE_BUCKET
        });
    }
}