module.exports = {
    es: {
        translation: {
            WELCOME_NEW_MSG: 'Hola! Bienvenido a Mis Bitcoins. ',
            WELCOME_MSG: 'Hola! Por lo que veo sigues teniendo %s bitcoins. ',
            REGISTER_MSG: 'Vale, %s bitcoins. Me lo apunto. ',
            SAY_MSG: 'Tienes %s bitcoins, lo que equivale a %s euros. ',
            MISSING_MSG: 'Parece que aun no me has dicho cuántos bitcoins tienes. Prueba decir: registra mis bitcoins. ',
            HELP_MSG: 'Puedes preguntarme: ¿cuánto valen mis bitcoins?, o registar el número de bitcoins que tienes diciendo: guarda mis bitcoins. ',
            GOODBYE_MSG: ['Hasta luego! ', 'Adios! ', 'Hasta pronto! ', 'Nos vemos! '],
            REFLECTOR_MSG: 'Acabas de activar %s ',
            FALLBACK_MSG: 'Lo siento, no se nada sobre eso. Por favor inténtalo otra vez. ',
            ERROR_MSG: 'Lo siento, ha habido un problema. Por favor inténtalo otra vez. ',
            UNKOWN_BITCOINS_MSG: 'Lo siento, no te he entendido, ¿cuántos bitcoins tienes? '
        }
    }   
}