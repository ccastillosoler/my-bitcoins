const axios = require('axios');

module.exports = {
    fetchBitcoinExchange(){
        const url = 'https://api.coindesk.com/v1/bpi/currentprice.json';

        var config = {
            timeout: 6500, 
            headers: {'Accept': 'application/json'}
        };

        async function getJsonResponse(url, config){
            const res = await axios.get(url, config);
            return res.data;
        }

        return getJsonResponse(url, config).then((result) => {
            return result;
        }).catch((error) => {
            console.log(JSON.stringify(error));
            return null;
        });
    },
    
    convertBitcoinExchangeResponse(handlerInput, response){
        return response.bpi.EUR.rate_float;
    }
}